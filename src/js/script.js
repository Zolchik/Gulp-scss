"use strict"

function changeMenuBar() {
    const icon = document.querySelector(".header-menu__bar--black .fas");
    document.querySelector(".header-menu").classList.toggle("opened");
    icon.classList.toggle("fa-bars");
    icon.classList.toggle("fa-times");
}

function activateLink(target) {
    const targetLink = target.tagName === "A" ? target : target.querySelector("a");
    console.log(targetLink);
    if (targetLink.classList.contains("active")) return;
    if (headerMenu.querySelector(".active")) {
        headerMenu.querySelector(".active").classList.remove("active");
    }
    targetLink.classList.add("active");
}

const headerMenu = document.querySelector(".header-menu");
headerMenu.addEventListener("click", (e) => {
    activateLink(e.target)
})
document.querySelector(".header-menu__bar--black").addEventListener("click", changeMenuBar);
